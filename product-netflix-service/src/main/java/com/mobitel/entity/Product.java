package com.mobitel.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
public class Product {

	@Id
	@GeneratedValue
	private Long productId;
	private String productName;
	private Integer quantity;
	private Double amount; 
	
}
