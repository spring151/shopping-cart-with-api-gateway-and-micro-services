package com.mobitel.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mobitel.entity.Product;
import com.mobitel.service.ProductService;


@RestController
@RequestMapping("/products")
public class ProductController {

	@Value("${product.message}")
	private String message;
	
	@Autowired
	ProductService productService;
	
	@PostMapping("/addproduct")
	@ResponseBody
	public Product addProduct(@RequestBody Product product) {
		return productService.addProduct(product);
	}
	
	@GetMapping("/loadproducts")
	public List<Product> listProduct() {
		System.out.println(message);
		return productService.loadProducts();
	}

	@GetMapping("/loadproducts/{plist}")
	public List<Product> listProductsByIds(@PathVariable List<Long> plist) {
		return productService.getByIds(plist);
	}
}
