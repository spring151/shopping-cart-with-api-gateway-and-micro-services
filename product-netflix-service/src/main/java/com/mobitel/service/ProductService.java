package com.mobitel.service;

import java.util.List;

import com.mobitel.entity.Product;

public interface ProductService {

	public Product addProduct(Product product);
	public List<Product> loadProducts();
	
	public List<Product> getByIds(List<Long> plist);
	
}
