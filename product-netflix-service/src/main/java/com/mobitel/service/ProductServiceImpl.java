package com.mobitel.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mobitel.entity.Product;
import com.mobitel.repo.ProductRepo;
@Service
public class ProductServiceImpl implements ProductService{

	@Autowired
	private ProductRepo repo;
	
	@Override
	public Product addProduct(Product product) {
		return repo.save(product);
		
	}

	@Override
	public List<Product> loadProducts() {
		return (List<Product>)repo.findAll();
	}

	@Override
	public List<Product> getByIds(List<Long> plist) {
		return (List<Product>)repo.findAllById(plist);
	}

}
