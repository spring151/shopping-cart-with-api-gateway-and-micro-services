package com.mobitel.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mobitel.entity.Product;

@Repository
public interface ProductRepo extends CrudRepository<Product,Long>{

}
