package com.mobitel.product.mobitelproductservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan("com")
@EntityScan("com.mobitel.entity")
@EnableJpaRepositories("com.mobitel.repo")
@EnableEurekaClient
public class ProductNetflixServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProductNetflixServiceApplication.class, args);
	}

}
