package com.mobitel.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class ShoppingApiGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShoppingApiGatewayApplication.class, args);
	}

	RouteLocator gatewayRoute(RouteLocatorBuilder builder) {
		return builder.routes()
				.route("sr",rs->rs
						.path("/shoppingcart/**")
						.uri("lb://SHOPPING_APP")
						).build();
	}
	
}