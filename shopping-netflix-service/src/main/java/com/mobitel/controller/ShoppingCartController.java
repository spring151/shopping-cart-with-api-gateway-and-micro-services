package com.mobitel.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mobitel.entity.ShoppingCartRequest;
import com.mobitel.entity.ShoppingCartResponse;
import com.mobitel.service.ShoppingService;

@RestController
@RequestMapping("/shoppingcart")
public class ShoppingCartController {

	@Autowired
	private ShoppingService service;

	@PostMapping("/{userId}/products")
	public ResponseEntity addProduct(@PathVariable Long userId, @RequestBody List<ShoppingCartRequest> reqProdList) {

		ShoppingCartResponse cartResponse = service.processAndRequest(userId, reqProdList);
		return new ResponseEntity<>(cartResponse, HttpStatus.CREATED);
	}
	
	@GetMapping("/{userId}")
	public ResponseEntity getShoppingCart(@PathVariable Long userId) {
		return ResponseEntity.ok(service.getShoppingCart(userId));

	}
	
	@GetMapping("/loadproducts")
	public ResponseEntity getProducts() {
		return ResponseEntity.ok(service.getProducts());

	}
}
