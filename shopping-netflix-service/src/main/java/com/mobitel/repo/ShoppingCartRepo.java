package com.mobitel.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mobitel.entity.CartEntity;
@Repository
public interface ShoppingCartRepo extends CrudRepository<CartEntity,Long>{

	public List<CartEntity> findByUserId(Long userId);
	
}
