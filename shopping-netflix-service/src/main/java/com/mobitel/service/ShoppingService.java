package com.mobitel.service;

import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import javax.management.RuntimeErrorException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mobitel.entity.CartEntity;
import com.mobitel.entity.Product;
import com.mobitel.entity.ShoppingCartRequest;
import com.mobitel.entity.ShoppingCartResponse;
import com.mobitel.repo.ShoppingCartRepo;

@Service
public class ShoppingService {

	@Autowired
	@Qualifier("webclient")
	private WebClient.Builder builder;
	
	@Autowired
	private ShoppingCartRepo repo;
	
	
	public ShoppingCartResponse processAndRequest(Long userId, List<ShoppingCartRequest> requests) {

		//call product api
		ObjectMapper mapper=new ObjectMapper();
		String productServiceUrl="http://product-app/products/loadproducts/"+requests.stream()
		
		.map(e->String.valueOf(e.getProductId())).collect(Collectors.joining(","));
		
		List<Product> productServiceList=builder.build()
				.get()
				.uri(productServiceUrl)
				.retrieve()
				.bodyToFlux(Product.class)
				.collectList()
				.block();
		
		System.out.println(productServiceUrl);
		System.out.println(productServiceList);
		
		//calculate cart value
		final Double[] totalCost= {0.0};
		
		productServiceList.forEach(pslr ->{
			requests.forEach(scr->{
				if(pslr.getProductId()==scr.getProductId()) {
					pslr.setQuantity(scr.getQuantity());
					totalCost[0]=totalCost[0]+pslr.getAmount()*scr.getQuantity();
				}
			});
		});
		
		//create cart entity
		CartEntity cartEntity=null;
		
		try {
			cartEntity=CartEntity.builder()
					.userId(userId)
					.cardId((long)(Math.random()*Math.pow(10, 10)))
					.totalItems(productServiceList.size())
					.totalCosts(totalCost[0])
					.products(mapper.writeValueAsString(productServiceList))
					.build();
				
		}catch(Exception e) {}
		
		//save cart in db
		cartEntity=repo.save(cartEntity);
		
		//create and return response
		ShoppingCartResponse response= ShoppingCartResponse.builder()
				.cardId(cartEntity.getCardId())
				.userId(cartEntity.getUserId())
				.totalItems(cartEntity.getTotalItems())
				.totalCosts(cartEntity.getTotalCosts())
				.products(productServiceList)
				.build();
		
		return response;
		
	}
	
	public List<ShoppingCartResponse> getShoppingCart(Long userId){
		ObjectMapper mapper=new ObjectMapper();
		List<CartEntity> cartEntities=repo.findByUserId(userId);
		
		List<ShoppingCartResponse> cartResponses=cartEntities.stream()
				.map(ce->{
					try {
						return ShoppingCartResponse.builder()
								.cardId(ce.getCardId())
								.userId(ce.getUserId())
								.totalItems(ce.getTotalItems())
								.totalCosts(ce.getTotalCosts())
								.products(mapper.readValue(ce.getProducts(), List.class))
								.build();
					}catch(JsonProcessingException e) {
						throw new RuntimeException(e);
					}
				}).collect(Collectors.toList());
		return cartResponses;
	}
	
	public List<Product> getProducts(){
		String productServiceUrl="http://product-app/products/loadproducts/";
		
		List<Product> productServiceList=builder.build()
				.get()
				.uri(productServiceUrl)
				.retrieve()
				.bodyToFlux(Product.class)
				.collectList()
				.block();
		return productServiceList;
	}
}
