package com.mobitel.product.mobitelproductservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.reactive.function.client.WebClient;

@SpringBootApplication
@ComponentScan("com")
@EntityScan("com")
@EnableJpaRepositories("com")
@EnableEurekaClient
public class ShoppingNetflixServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShoppingNetflixServiceApplication.class, args);
	}

	@LoadBalanced
	@Bean("webclient")
	public WebClient.Builder getWebClient(){
		return WebClient.builder();
	}
}
