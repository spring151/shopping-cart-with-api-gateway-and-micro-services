package com.mobitel.entity;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ShoppingCartResponse {

	private long userId;
	private Long cardId;
	private Integer totalItems;
	private Double totalCosts;
	private List<Product> products;
}
