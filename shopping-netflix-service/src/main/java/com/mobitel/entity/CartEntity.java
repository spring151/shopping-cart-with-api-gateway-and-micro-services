package com.mobitel.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class CartEntity {

	@Id
	private long userId;
	private Long cardId;
	private Integer totalItems;
	private Double totalCosts;
	private String products;
}
